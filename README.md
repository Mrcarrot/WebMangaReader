# WebMangaReader

This is a very basic self-hosted solution for reading manga(or viewing images in general) in a mobile browser. It also works on desktop with a 2-page view.

To get started:
* Install a web server of your choice. Just about any that supports HTTP(basically all of them) will do.
* Place Index.html, main.js, and main.css in whichever location your web server uses for its root directory. Alternatively, you can place them all in a separate directory labeled "WebMangaReader" or similar if you want to access it at a different location.
* Place another folder called Images next to the three files you installed earlier. This is where the reader will get its images from.
* Inside the Images folder, add as many images as you want. The name of each image must be a number, starting at 1 and ascending in order of page number. For example, if the images are JPGs, page 204 should be 204.jpg.
  * If you are using a different image type, such as PNG, you will need to modify the top line of main.js to recognize that extension.
* In a browser, navigate to the address. If JavaScript is activated, the reader should start working immediately.

FAQ:
* Q: Why should I use this over something like Tachiyomi?
  * A: If you're asking this question, you probably shouldn't. If, however, like me, you are tired of installing an app for everything and want a simple solution to read a series on your phone or desktop computer, this may be worth looking into.
* Q: Does this support x feature?
  * A: Probably not, but additional features may be added in the future. You can also feel free to submit a pull request. At the moment, this is a very simple site that displays images in sequential order and remembers the last one you viewed.