const fileExtension = ".jpg" //Change if using files with an extension other than .jpg

var currentPage = 1;

function firstRunWidthCheck() {
    if (getCookie("width-check-already-run") == "true") return;
    if((window.innerWidth || document.documentElement.clientWidth) > 850) {
        window.location.assign("2Pages.html");
    }
    else {
        window.location.assign("Index.html");
    }
    setCookie("width-check-already-run", "true", 365);
}

updatePage = function () {
    document.getElementById("manga_page").src = "Images/" + currentPage.toString() + fileExtension;
    document.getElementById("page_number_input").value = currentPage;
    setCookie("page", currentPage.toString(), 30); //Remembers the page for the next 30 days
}

goToPage = function (x) {
    currentPage = x;
    updatePage();
}

nextPage = function () {
    currentPage++;
    updatePage();
}

previousPage = function () {
    if (currentPage > 1) currentPage--;
    updatePage();
}

jumpToPage = function () {
    currentPage = parseInt(document.getElementById("page_number_input").value);
    updatePage();
}

update2Page = function () {
    document.getElementById("manga_page").src = "Images/" + currentPage.toString() + fileExtension;
    document.getElementById("manga_page_2").src = "Images/" + (currentPage + 1).toString() + fileExtension;
    document.getElementById("page_number_input").value = currentPage;
    setCookie("page", currentPage.toString(), 30); //Remembers the page for the next 30 days
}

goTo2Page = function (x) {
    currentPage = x;
    update2Page();
}

next2Page = function () {
    currentPage += 2;
    update2Page();
}

previous2Page = function () {
    if (currentPage > 2) currentPage -= 2;
    update2Page();
}

jumpTo2Page = function () {
    currentPage = parseInt(document.getElementById("page_number_input").value);
    update2Page();
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

pageLoad = function () {
    if (getCookie("page") != "") {
        currentPage = parseInt(getCookie("page"));
    }
    updatePage();
}

pageLoad2 = function () {
    if (getCookie("page") != "") {
        currentPage = parseInt(getCookie("page"));
    }
    update2Page();
}